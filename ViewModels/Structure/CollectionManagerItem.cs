﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModels.Base;

namespace ViewModels.Structure
{
    public abstract class CollectionManagerItem : ViewModel, IDisposable
    {
        ~CollectionManagerItem()
        {
            // See: http://stackoverflow.com/a/538238/311207 for `Dispose` methodology
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public virtual void Dispose(bool manuallyInvoked)
        {

        }
    }
}

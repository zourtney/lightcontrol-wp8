﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModels.Base;
using ViewModels.Interfaces;

namespace ViewModels.Structure
{
    public abstract class CollectionManager<T> : CollectionViewModel<T>, ICollectionManager, IDisposable
        where T : ViewModel
    {
        protected bool _isLoading;
        protected bool _isEditable;
        protected DelegateCommand _refreshCommand;

        public CollectionManager()
        {
            _isLoading = false;
            _isEditable = false;

            _refreshCommand = new DelegateCommand(OnRefresh);

            Mediator.Register("LongOperationStarted", LongOperationStarted);
            Mediator.Register("LongOperationFinished", LongOperationFinished);
        }

        ~CollectionManager()
        {
            // See: http://stackoverflow.com/a/538238/311207 for `Dispose` methodology
            Dispose(false);
        }

        public virtual void Dispose()
        {
            Dispose(true);
        }

        public virtual void Dispose(bool manuallyInvoked)
        {
            Mediator.Unregister("LongOperationStarted", LongOperationStarted);
            Mediator.Unregister("LongOperationFinished", LongOperationFinished);
        }

        #region Commands
        public ICommand Refresh { get { return _refreshCommand; } }
        protected abstract void OnRefresh(object sender);
        #endregion

        #region Mediator Callbacks
        protected virtual void LongOperationStarted(object sender)
        {
            IsLoading = true;
            IsEditable = false;
        }

        protected virtual void LongOperationFinished(object sender)
        {
            IsLoading = false;
            IsEditable = true;
        }
        #endregion

        #region Properties
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                _isLoading = value;
                NotifyPropertyChanged("IsLoading");
            }
        }

        public bool IsEditable
        {
            get
            {
                return _isEditable;
            }
            set
            {
                _isEditable = value;
                NotifyPropertyChanged("IsEditable");
            }
        }
        #endregion
    }
}

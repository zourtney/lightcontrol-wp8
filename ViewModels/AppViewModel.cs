﻿using Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Services.Base;
using ViewModels.Structure;
using ViewModels.Base;
using System.Windows.Input;
using ViewModels.Schedules;
using ViewModels.Outlets;

namespace ViewModels
{
    public class AppViewModel : ViewModel
    {
        private OutletsViewModel _outlets;
        private SchedulesViewModel _schedules;

        public AppViewModel()
        {
            _outlets = new OutletsViewModel();
            _schedules = new SchedulesViewModel(_outlets);
            Mediator.NotifyColleagues("FetchOutlets", this);
        }

        #region Properties
        public Uri FirstPage
        {
            get
            {
                if (LocalSettings.RunTutorial)
                    return new Uri("/Tutorial.xaml", UriKind.Relative);
                return new Uri("/MainPage.xaml", UriKind.Relative);
            }
        }

        public OutletsViewModel Outlets
        {
            get
            {
                return _outlets;
            }
        }

        public SchedulesViewModel Schedules
        {
            get
            {
                return _schedules;
            }
        }
        #endregion
    }
}
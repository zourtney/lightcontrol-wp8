﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModels.Base;
using ViewModels.Interfaces;
using ViewModels.Structure;

namespace ViewModels.Outlets
{
    public class OutletViewModel : CollectionManagerItem, IOutletViewModel
    {
        protected Outlet _model;
        protected bool _isEnabled;
        protected bool? _isOn = null;

        public static int DROPDOWN_NO_CHANGE = 0;
        public static int DROPDOWN_ON = 1;
        public static int DROPDOWN_OFF = 2;

        public OutletViewModel(Outlet outlet)
        {
            _model = outlet;
            IsEnabled = true;
            SetIsOn(_model.Value == Outlet.ON, false, false);
        }

        /// <summary>
        /// Creates a new copy of this OutletViewModel as an OfflineOutletViewModel.
        /// This copy is not attached to a server-side model, which is useful for
        /// making observable outlet objects on schedules.
        /// </summary>
        /// <returns></returns>
        public OfflineOutletViewModel CloneAsOffline()
        {
            var vm = new OfflineOutletViewModel();
            vm.ID = ID;
            vm.IsEnabled = IsEnabled;
            vm.IsOn = IsOn;
            vm.OriginalIsOn = IsOn;
            return vm;
        }

        public async void SetIsOn(bool? val, bool save, bool notify)
        {
            if (!val.HasValue)
            {
                _isOn = null;
            }
            else
            {
                _model.Value = val.Value ? Outlet.ON : Outlet.OFF;
                _isOn = _model.Value == Outlet.ON;

                if (save)
                {
                    try
                    {
                        Mediator.NotifyColleagues("LongOperationStarted", this);
                        IsEnabled = false;
                        await _model.Save();
                        Mediator.NotifyColleagues("OutletValueChanged", this);
                        Mediator.NotifyColleagues("LongOperationFinished", this);
                    }
                    catch
                    {
                        //TODO: pass along actual error
                        //if (SaveError != null)
                        //    SaveError(this, EventArgs.Empty);
                        throw new Exception("Oh dear");
                    }
                    finally
                    {
                        IsEnabled = true;
                    }
                }
            }

            if (notify)
            {
                NotifyPropertyChanged("IsOn");
                NotifyPropertyChanged("ValueAsIndex");
            }
        }

        #region Properties
        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                _isEnabled = value;
                NotifyPropertyChanged("IsEnabled");
            }
        }

        public string ID
        {
            get
            {
                return _model.ID;
            }
        }

        public bool? IsOn {
            get
            {
                if (_isOn.HasValue)
                    return _model.Value == Outlet.ON;
                return null;
            }
            set
            {
                SetIsOn(value, true, true);
            }
        }

        public int ValueAsIndex
        {
            get
            {
                if (!IsOn.HasValue)
                    return DROPDOWN_NO_CHANGE;
                return IsOn.Value ? DROPDOWN_ON : DROPDOWN_OFF;
            }
            set
            {
                if (value == DROPDOWN_NO_CHANGE)
                    IsOn = null;
                else
                    IsOn = value == DROPDOWN_ON;
            }
        }
        #endregion


        #region Static Converters
        //TODO: make other things use this
        public static bool? ValueToBool(int val)
        {
            if (val == Outlet.ON)
                return true;
            if (val == Outlet.OFF)
                return false;
            return null;
        }

        public static int BoolToValue(bool? val)
        {
            if (!val.HasValue)
                return -1;
            if (val.Value)
                return Outlet.ON;
            return Outlet.OFF;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModels.Base;
using ViewModels.Interfaces;
using ViewModels.Structure;

namespace ViewModels.Outlets
{
    public class OfflineOutletViewModel : CollectionManagerItem, IOutletViewModel
    {
        private bool _isEnabled;
        private string _id;
        private bool? _isOn;
        private bool? _originalIsOn;

        public IOutletViewModel CloneAsOffline()
        {
            throw new NotImplementedException();
        }

        #region Properties
        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                _isEnabled = value;
                NotifyPropertyChanged("IsEnabled");
            }
        }

        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                NotifyPropertyChanged("ID");
            }
        }

        public bool? IsOn
        {
            get
            {
                return _isOn;
            }
            set
            {
                _isOn = value;
                NotifyPropertyChanged("IsOn");
                NotifyPropertyChanged("ValueAsIndex");
            }
        }

        public bool? OriginalIsOn
        {
            get
            {
                return _originalIsOn;
            }
            set
            {
                _originalIsOn = value;
                NotifyPropertyChanged("OriginalIsOn");
            }
        }

        public int ValueAsIndex
        {
            get
            {
                if (!IsOn.HasValue)
                    return OutletViewModel.DROPDOWN_NO_CHANGE;
                return IsOn.Value ? OutletViewModel.DROPDOWN_ON : OutletViewModel.DROPDOWN_OFF;
            }
            set
            {
                if (value == OutletViewModel.DROPDOWN_NO_CHANGE)
                    IsOn = null;
                else
                    IsOn = value == OutletViewModel.DROPDOWN_ON;
            }
        }
        #endregion
    }
}

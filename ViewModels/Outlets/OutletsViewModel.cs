﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModels.Base;
using ViewModels.Structure;

namespace ViewModels.Outlets
{
    public class OutletsViewModel : CollectionManager<OutletViewModel>
    {
        protected OutletCollection _collection;
        protected int _onOutletCount;

        public OutletsViewModel()
        {
            Mediator.Register("OutletValueChanged", OnOutletValueChanged);
            Mediator.Register("FetchOutlets", OnFetchRequested);
        }

        public override void Dispose(bool manuallyInvoked)
        {
            base.Dispose(manuallyInvoked);

            Mediator.Unregister("OutletValueChanged", OnOutletValueChanged);
            Mediator.Unregister("FetchOutlets", OnFetchRequested);

            foreach (var item in this)
                item.Dispose();
        }

        #region Helpers
        private void TallyCollection()
        {
            // Loop through and see if we can prove false that entirety of the
            // collection is "on".
            //
            // NOTE: was original implemented with TakeWhile, which is awesome.
            // http://stackoverflow.com/a/7053503/311207
            _onOutletCount = 0;
            foreach (var curItem in this)
            {
                if (curItem.IsOn.Value)
                    _onOutletCount++;
            }

            NotifyPropertyChanged("IsFullyOn");
            NotifyPropertyChanged("IsOnLabelText");
        }

        private async void SetAllOutlets(bool val)
        {
            Mediator.NotifyColleagues("LongOperationStarted", this);

            foreach (var item in this)
            {
                // Set the individual outlet value, but don't save it. We
                // DO trigger a property change notification, but it
                // shouldn't be too expensive since the the listener will
                // skip it when `item.IsOn.Value == IsOn`.
                item.SetIsOn(val, false, true);
            }

            await _collection.Save();

            // Update "IsFullyOn", etc
            TallyCollection();

            Mediator.NotifyColleagues("LongOperationFinished", this);
        }
        #endregion

        #region Commands
        protected override async void OnRefresh(object sender)
        {
            try
            {
                IsEditable = false;
                Mediator.NotifyColleagues("LongOperationStarted", this);

                var newCollection = new OutletCollection(LocalSettings.ServerAddress);
                if (await newCollection.Fetch(LocalSettings.NumRetries))
                {
                    // Clear out the old view models
                    foreach (var item in this)
                        item.Dispose();
                    ClearItems();

                    // Create the new ones
                    foreach (var model in newCollection.OrderBy(outlet => outlet.Value.ID))
                        Add(new OutletViewModel(model.Value));
                    
                    // Update "IsFullyOn", etc
                    TallyCollection();

                    _collection = newCollection;
                    IsEditable = true;
                    LocalSettings.IsVerified = true;
                }
                else
                {
                    LocalSettings.IsVerified = false;
                    IsEditable = false;
                    Mediator.NotifyColleagues("FetchOutletsFailed", this);
                }
            }
            finally
            {
                Mediator.NotifyColleagues("LongOperationFinished", this);
            }
        }
        #endregion

        #region Mediator Callbacks
        private void OnFetchRequested(object sender)
        {
            Refresh.Execute(null);
        }

        private void OnOutletValueChanged(object outlet)
        {
            //var item = outlet as OutletViewModel;
            //if (item != null && item.IsOn.Value != IsFullyOn)
            TallyCollection();
        }
        #endregion

        #region Properties
        public bool IsFullyOn
        {
            get
            {
                if (_onOutletCount == 0)  // So that 0/0 doesn't appear "on"
                    return false;
                return _onOutletCount == Count;
            }
            set
            {
                SetAllOutlets(value);
            }
        }

        public string IsOnLabelText
        {
            get
            {
                if (_onOutletCount == 0)
                    return "All outlets are OFF";
                if (_onOutletCount == Count)
                    return "All outlets are ON";
                return "Some outlets are ON";
            }
        }
        #endregion
    }
}

﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModels.Base;

namespace ViewModels.Settings
{
    public class SettingsViewModel : ViewModel
    {
        public SettingsViewModel()
        { }

        #region Properties
        public string ServerAddress
        {
            get
            {
                return LocalSettings.ServerAddress;
            }
            set
            {
                LocalSettings.ServerAddress = value;
                NotifyPropertyChanged("ServerAddress");
                NotifyPropertyChanged("IsVerifiedPretty");
            }
        }

        public int NumRetries
        {
            get
            {
                return LocalSettings.NumRetries;
            }
            set
            {
                LocalSettings.NumRetries = value;
                NotifyPropertyChanged("NumRetries");
                NotifyPropertyChanged("IsVerifiedPretty");
            }
        }

        public string IsVerifiedPretty
        {
            get
            {
                return LocalSettings.IsVerified ? "Verified" : "Not verified";
            }
        }
        #endregion
    }
}

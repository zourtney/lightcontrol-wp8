﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels.Interfaces
{
    public interface IOutletViewModel
    {
        bool IsEnabled { get; set; }
        string ID { get; }
        bool? IsOn { get; set; }
        int ValueAsIndex { get; set; }
    }
}

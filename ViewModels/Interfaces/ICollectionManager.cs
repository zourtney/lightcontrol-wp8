﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ViewModels.Interfaces
{
    public interface ICollectionManager
    {
        ICommand Refresh { get; }

        bool IsLoading { get; }
        bool IsEditable { get; }
    }
}

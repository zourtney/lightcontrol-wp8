﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace ViewModels.Base
{
    public class ViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Notifies of changed class property values. Used to keep UI in sync with the ViewModel.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Dispatches a "PropertyChanged" event for any given string
        /// </summary>
        /// <param name="propertyName">Name of the property to dispatch a change event on</param>
        protected void NotifyPropertyChanged(String propertyName)
        {
            // Avoid `UnauthorizedAccessException` exceptions
            // http://info.titodotnet.com/2012/02/unauthorized-access-exception-when.html
            if (PropertyChanged != null)
            {
                if (Deployment.Current.Dispatcher.CheckAccess())
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                else
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                    });
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ViewModels.Base
{
    public class CollectionViewModel<T> : ObservableCollection<T>
    {
        /// <summary>
        /// Dispatches a "PropertyChanged" event for any given string
        /// </summary>
        /// <param name="propertyName">Name of the property to dispatch a change event on</param>
        protected void NotifyPropertyChanged(String propertyName)
        {
            // Avoid `UnauthorizedAccessException` exceptions
            // http://info.titodotnet.com/2012/02/unauthorized-access-exception-when.html
            if (Deployment.Current.Dispatcher.CheckAccess())
            {
                OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
                });
            }
        }
    }
}

﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModels.Base;

namespace ViewModels.Tutorial
{
    public class TutorialViewModel : ViewModel
    {
        private int _curPage = 0;
        private int _numPages = 0;

        private DelegateCommand _previousCommand; 
        private DelegateCommand _nextCommand;
        private DelegateCommand _skipCommand;
        
        public TutorialViewModel()
        {
            _previousCommand = new DelegateCommand(OnMovePrevious);
            _nextCommand = new DelegateCommand(OnMoveNext);
            _skipCommand = new DelegateCommand(OnSkip);
        }

        #region Commands
        public ICommand MovePrevious { get { return _previousCommand; } }
        private void OnMovePrevious(object obj)
        {
            if (HasPrevious)
            {
                --CurPage;
                Mediator.NotifyColleagues("MovePreviousTutorialPage", CurPage);
            }
        }

        public ICommand MoveNext { get { return _nextCommand; } }
        private void OnMoveNext(object obj)
        {
            if (HasNext)
            {
                CurPage++;
                Mediator.NotifyColleagues("MoveNextTutorialPage", CurPage);
            }
            else
            {
                LocalSettings.RunTutorial = false; // don't run tutorial on next app start
                Mediator.NotifyColleagues("FinishTutorial", CurPage);
            }
        }

        public ICommand Skip { get { return _skipCommand; } }
        private void OnSkip(object obj)
        {
            LocalSettings.RunTutorial = false; // don't run tutorial on next app start
            Mediator.NotifyColleagues("SkipTutorial", CurPage);
        }
        #endregion

        #region Properties
        public bool HasPrevious
        {
            get
            {
                return _curPage > 1; // assumes blank, unreachable page 0 (for transitions)
            }
        }

        public bool HasNext
        {
            get
            {
                return _curPage < _numPages - 1;
            }
        }

        public string NextButtonText
        {
            get
            {
                if (HasNext)
                    return "next";
                return "finish";
            }
        }

        public int CurPage
        {
            get
            {
                return _curPage;
            }
            private set
            {
                _curPage = value;
                NotifyPropertyChanged("CurPage");
                NotifyPropertyChanged("HasPrevious");
                NotifyPropertyChanged("HasNext");
                NotifyPropertyChanged("NextButtonText");
            }
        }

        public int NumPages
        {
            get
            {
                return _numPages;
            }
            set
            {
                _numPages = value;

                // Again, there's probably a better way to do this. (Is that what "dependency properties" are about?)
                NotifyPropertyChanged("NumPages");
                NotifyPropertyChanged("CurPage");
                NotifyPropertyChanged("HasPrevious");
                NotifyPropertyChanged("HasNext");
                NotifyPropertyChanged("NextButtonText");
            }
        }

        public string SkipMessage
        {
            get
            {
                return "You can launch the tutorial again at any time from the \"about\" screen. Enjoy!";
            }
        }

        public string SkipMessageTitle
        {
            get
            {
                return "Skipping tutorial";
            }
        }
        #endregion
    }
}

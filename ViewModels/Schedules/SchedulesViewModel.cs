﻿using Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModels.Base;
using ViewModels.Interfaces;
using ViewModels.Outlets;
using ViewModels.Structure;

namespace ViewModels.Schedules
{
    public class SchedulesViewModel : CollectionManager<ScheduleViewModel>
    {
        private OutletsViewModel _outlets;   // needed to generate list of editable outlets on each ScheduleViewModel

        public SchedulesViewModel(OutletsViewModel outlets)
        {
            Mediator.Register("FetchSchedules", OnFetchRequested);
            Mediator.Register("ScheduleCreated", OnScheduleCreated);
            Mediator.Register("ScheduleDestroyed", OnScheduleDestroyed);
            _outlets = outlets;
        }

        public override void Dispose(bool manuallyInvoked)
        {
            base.Dispose(manuallyInvoked);
            
            Mediator.Unregister("FetchSchedules", OnFetchRequested);
            Mediator.Unregister("ScheduleCreated", OnScheduleCreated);
            Mediator.Unregister("ScheduleDestroyed", OnScheduleDestroyed);

            foreach (var item in this)
                item.Dispose();
        }

        /// <summary>
        /// Creates a new unattached schedule model and viewmodel
        /// </summary>
        /// <returns></returns>
        public ScheduleViewModel CreateNew()
        {
            try
            {
                var schedule = new Schedule(LocalSettings.ServerAddress, CreateDefaultScheduleName(), "");
                var scheduleVM = new ScheduleViewModel(schedule, _outlets);
                return scheduleVM;
            }
            catch
            {
                return null;
            }
        }
        
        #region Commands
        protected override async void OnRefresh(object sender)
        {
            try
            {
                Mediator.NotifyColleagues("LongOperationStarted", this);

                var newCollection = new ScheduleCollection(LocalSettings.ServerAddress);
                if (await newCollection.Fetch(LocalSettings.NumRetries))
                {
                    // Clean up old view models
                    foreach (var item in this)
                        item.Dispose();
                    ClearItems();

                    // Create new ones
                    var list = from model in newCollection.Values
                               orderby model.Next
                               select new ScheduleViewModel(model, _outlets);

                    foreach (var scheduleVM in list)
                        Add(scheduleVM);

                    LocalSettings.IsVerified = true;
                }
                else
                {
                    Mediator.NotifyColleagues("FetchSchedulesFailed", this);
                }
            }
            finally
            {
                Mediator.NotifyColleagues("LongOperationFinished", this);
            }
        }
        #endregion

        #region Mediator Callbacks
        private void OnFetchRequested(object sender)
        {
            Refresh.Execute(null);
        }

        private void OnScheduleCreated(object obj)
        {
            Add(obj as ScheduleViewModel);
        }

        private void OnScheduleDestroyed(object obj)
        {
            Remove(obj as ScheduleViewModel);
        }
        #endregion

        #region Helpers
        private string CreateDefaultScheduleName()
        {
            var num = Count;
            string name;

            do
            {
                name = "Schedule " + (++num).ToString();
            }
            while (this.Any(s => s.Name == name));

            return name;
        }
        #endregion
    }
}

﻿using Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ViewModels.Base;
using ViewModels.Interfaces;
using ViewModels.Outlets;
using ViewModels.Structure;

namespace ViewModels.Schedules
{
    public class ScheduleViewModel : CollectionManagerItem
    {
        private Schedule _model;
        private ObservableCollection<OfflineOutletViewModel> _outlets;
        private bool _isLoading;
        private bool _isEditable;

        private string _originalName;
        private string _originalCron;

        private DelegateCommand _selectCommand;
        private DelegateCommand _saveCommand;
        private DelegateCommand _requestedDeleteCommand; 
        private DelegateCommand _deleteCommand;
        private DelegateCommand _revertCommand;
        

        public ScheduleViewModel(Schedule schedule, OutletsViewModel allOutletsVMs)
        {
            _model = schedule;
            _outlets = new ObservableCollection<OfflineOutletViewModel>();

            _originalName = Name;
            _originalCron = Cron;

            _selectCommand = new DelegateCommand(OnSelectSchedule);
            _saveCommand = new DelegateCommand(OnSave);
            _requestedDeleteCommand = new DelegateCommand(OnRequestedDelete);
            _deleteCommand = new DelegateCommand(OnDelete);
            _revertCommand = new DelegateCommand(OnRevert);

            foreach (var outletVM in allOutletsVMs)
            {
                var offlineVM = outletVM.CloneAsOffline();
                var scheduleOutletModel = _model.Outlets.Find(outlet => { return outlet.ID == outletVM.ID; });
                
                if (scheduleOutletModel == null)
                    offlineVM.IsOn = null;
                else
                    offlineVM.IsOn = OutletViewModel.ValueToBool(scheduleOutletModel.Value);

                offlineVM.OriginalIsOn = offlineVM.IsOn;
                
                _outlets.Add(offlineVM);
            }

            Mediator.Register("LongOperationStarted", OnLongOperationStarted);
            Mediator.Register("LongOperationFinished", OnLongOperationFinished);

            IsLoading = false;
            IsEditable = true;
        }

        public override void Dispose(bool manuallyInvoked)
        {
            Mediator.Unregister("LongOperationStarted", OnLongOperationStarted);
            Mediator.Unregister("LongOperationFinished", OnLongOperationFinished);
        }

        #region Mediator Callbacks
        private void OnLongOperationStarted(object sender)
        {
            IsLoading = true;
            IsEditable = false;
        }

        private void OnLongOperationFinished(object sender)
        {
            IsLoading = false;
            IsEditable = true;
        }
        #endregion

        #region Commands
        public ICommand SelectSchedule { get { return _selectCommand; } }
        private void OnSelectSchedule(object sender)
        {
            Mediator.NotifyColleagues("ShowSchedule", this);
        }

        public ICommand Save { get { return _saveCommand; } }
        private async void OnSave(object sender)
        {
            Mediator.NotifyColleagues("LongOperationStarted", this);

            try
            {
                // Update `_model.Outlets`
                foreach (var outletVM in Outlets)
                {
                    var outletModel = _model.Outlets.Find(o => { return o.ID == outletVM.ID; });

                    if (outletVM.IsOn.HasValue)
                    {
                        // "no change" --> "on" or "off"
                        if (outletModel == null)
                        {
                            //TODO: new Outlet(LocalStorage.ServerAddress, outletVM.ID)
                            outletModel = new Outlet("");
                            outletModel.ID = outletVM.ID;
                            _model.Outlets.Add(outletModel);
                        }

                        outletModel.Value = OutletViewModel.BoolToValue(outletVM.IsOn);
                    }
                    else if (outletModel != null)
                    {
                        // Existing model set to "no change"
                        _model.Outlets.Remove(outletModel);
                    }
                }

                // Store 'new' state until after the save operation. We don't
                // want to stick it in the viewmodel collection until we it has
                // successfully saved.
                var wasNew = _model.IsNew;
                await _model.Save();
                if (wasNew)
                    Mediator.NotifyColleagues("ScheduleCreated", this);
                
                // Update the cached "revertable to" values
                _originalName = Name;
                _originalCron = Cron;
                foreach (var item in _outlets)
                    item.OriginalIsOn = item.IsOn;
                
                // There might be a better way to do this, but...meh.
                NotifyPropertyChanged("IsNew");
                NotifyPropertyChanged("Name");
                NotifyPropertyChanged("Enabled");
                NotifyPropertyChanged("Cron");
                NotifyPropertyChanged("Next");
                NotifyPropertyChanged("Outlets");
                NotifyPropertyChanged("ScheduledOutlets");
                NotifyPropertyChanged("OutletsCompact");

                Mediator.NotifyColleagues("ScheduleSaved", this);
            }
            catch
            {
                //TODO: pass along exception
                Mediator.NotifyColleagues("ScheduleSaveFailed", this);
            }

            Mediator.NotifyColleagues("LongOperationFinished", this);
        }

        public ICommand RequestDelete { get { return _requestedDeleteCommand; } }
        private void OnRequestedDelete(object sender)
        {
            Mediator.NotifyColleagues("ScheduleDestroyRequested", this);
        }

        public ICommand Delete { get { return _deleteCommand; } }
        private async void OnDelete(object sender)
        {
            try
            {
                Mediator.NotifyColleagues("LongOperationStarted", this);

                await _model.Delete();

                // Remove from viewmodel collection
                Mediator.NotifyColleagues("ScheduleDestroyed", this);
            }
            catch
            {
                //TODO: pass along exception
                Mediator.NotifyColleagues("ScheduleDestroyFailed", this);
            }

            Mediator.NotifyColleagues("LongOperationFinished", this);
        }

        public ICommand Revert { get { return _revertCommand; } }
        private void OnRevert(object sender)
        {
            Name = _originalName;
            Cron = _originalCron;
            foreach (var item in _outlets)
                item.IsOn = item.OriginalIsOn;
        }
        #endregion

        #region Properties
        public bool IsNew
        {
            get
            {
                return _model.IsNew;
            }
        }
        
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            private set
            {
                _isLoading = value;
                NotifyPropertyChanged("IsLoading");
            }
        }

        public bool IsEditable
        {
            get
            {
                return _isEditable;
            }
            private set
            {
                _isEditable = value;
                NotifyPropertyChanged("IsEditable");
            }
        }

        public string Name
        {
            get
            {
                return _model.ID;
            }
            set
            {
                _model.ID = value;
                NotifyPropertyChanged("Name");
            }
        }

        public bool Enabled
        {
            get
            {
                return _model.Enabled;
            }
            set
            {
                _model.Enabled = value;
                NotifyPropertyChanged("Enabled");
            }
        }

        public string Cron
        {
            get
            {
                return _model.Cron;
            }
            set
            {
                _model.Cron = value;
                NotifyPropertyChanged("Cron");
            }
        }

        public string Next
        {
            get
            {
                return _model.Next.ToString();
            }
            set
            {
                _model.Next = DateTime.Parse(value);
                NotifyPropertyChanged("Next");
            }
        }

        public ObservableCollection<OfflineOutletViewModel> Outlets
        {
            get
            {
                return _outlets;
            }
        }

        public ObservableCollection<OfflineOutletViewModel> ScheduledOutlets
        {
            get
            {
                return new ObservableCollection<OfflineOutletViewModel>(
                    from outletVM in _outlets
                    where outletVM.IsOn.HasValue
                    select outletVM
                );
            }
        }

        public string OutletsCompact
        {
            get
            {
                StringBuilder str = new StringBuilder();
                foreach (var outlet in ScheduledOutlets)
                {
                    str.Append(outlet.ID);
                    str.Append(outlet.IsOn.Value ? "=on; " : "=off; ");
                }
                return str.ToString();
            }
        }

        public string EditScreenTitle
        {
            get
            {
                if (_model.OriginalID.Length > 0)
                    return "edit schedule";
                return "new schedule";
            }
        }

        public string RequestDeleteTitleText
        {
            get
            {
                return "Delete schedule?";
            }
        }

        public string RequestDeleteContextText
        {
            get
            {
                return "Are you sure you want to delete \"" + Name + "\"? This action cannot be undone.";
            }
        }
        #endregion
    }
}

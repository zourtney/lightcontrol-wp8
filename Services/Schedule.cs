﻿using Newtonsoft.Json;
using Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class Schedule : BaseModel<ScheduleJsonConverter>
    {
        public Schedule(string baseUrl)
            : base(baseUrl)
        { }

        public Schedule(string baseUrl, string id, string originalID = "")
            : base(baseUrl)
        {
            ID = id;
            OriginalID = originalID;
            Outlets = new List<Outlet>();
        }

        #region Properties
        public string OriginalID { get; set; }

        public override bool IsNew
        {
            get { return OriginalID.Length < 1; }
        }

        public override string Url
        {
            get { return "schedules/" + (IsNew ? "" : OriginalID + "/") + "?t=" + DateTime.Now.Ticks; }
        }

        [JsonProperty("name")]
        public override string ID { get; set; }

        [JsonProperty("cron")]
        public string Cron { get; set; }

        [JsonProperty("next")]
        public DateTime Next { get; set; }

        [JsonProperty("enabled")]
        public bool Enabled { get; set; }

        [JsonProperty("outlets")]
        public List<Outlet> Outlets { get; set; }
        #endregion
    }
}

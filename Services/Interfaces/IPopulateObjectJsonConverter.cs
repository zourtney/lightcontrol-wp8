﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IPopulateObjectJsonConverter
    {
        void PopulateObject(string content, object target);
    }
}

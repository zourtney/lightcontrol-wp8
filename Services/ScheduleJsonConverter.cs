﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Services.Base;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ScheduleJsonConverter : JsonConverter, IPopulateObjectJsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(Schedule).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);
            var schedule = new Schedule("");
            serializer.Populate(jObject.CreateReader(), schedule);
            schedule.OriginalID = schedule.ID;
            return schedule;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public void PopulateObject(string content, object target)
        {
            var targetSchedule = target as Schedule;
            var sourceSchedule = JsonConvert.DeserializeObject<Schedule>(content, this);
            targetSchedule.Outlets.Clear();
            JsonConvert.PopulateObject(content, targetSchedule);
            targetSchedule.OriginalID = sourceSchedule.ID;
        }
    }
}

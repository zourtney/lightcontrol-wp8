﻿using Newtonsoft.Json;
using Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ScheduleCollection : BaseCollection<Schedule, ScheduleJsonConverter>
    {
        public ScheduleCollection(string baseUrl)
            : base(baseUrl)
        { }

        public override Task<bool> Save()
        {
            throw new NotImplementedException();
        }

        #region Properties
        public override string Url
        {
            get { return "schedules/?t=" + DateTime.Now.Ticks; }
        }
        #endregion
    }
}

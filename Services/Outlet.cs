﻿using Newtonsoft.Json;
using Services.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class Outlet : BaseModel
    {
        public static int ON = 0;
        public static int OFF = 1;

        public Outlet(string baseUrl)
            : base(baseUrl)
        { }

        #region Properties
        public override string Url
        {
            get { return "outlets/" + ID + "/?t=" + DateTime.Now.Ticks; }
        }

        [JsonProperty("value")]
        public int Value { get; set; }
        #endregion
    }
}

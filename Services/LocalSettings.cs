﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public static class LocalSettings
    {
        private static IsolatedStorageSettings _userSettings = IsolatedStorageSettings.ApplicationSettings;

        static LocalSettings()
        {
            RunTutorial = GetUserSetting<bool>("runTutorial", true);
            IsVerified = GetUserSetting<bool>("isVerified", false);
            NumRetries = GetUserSetting<int>("numRetries", 3);
            ServerAddress = GetUserSetting<string>("serverAddress", "http://10.0.1.3:5000");
        }

        #region Helpers
        private static T GetUserSetting<T>(string name, T defaultValue)
        {
            try
            {
                return (T)_userSettings[name];
            }
            catch
            {
                return defaultValue;
            }
        }
        #endregion

        #region Properties
        public static bool RunTutorial
        {
            get
            {
                return (bool)_userSettings["runTutorial"];
            }
            set
            {
                _userSettings["runTutorial"] = value;
            }
        }

        public static string ServerAddress
        {
            get
            {
                return (string)_userSettings["serverAddress"];
            }
            set
            {
                _userSettings["serverAddress"] = value;
            }
        }

        public static int NumRetries
        {
            get
            {
                return (int)_userSettings["numRetries"];
            }
            set
            {
                _userSettings["numRetries"] = value;
            }
        }

        public static bool IsVerified
        {
            get
            {
                return (bool)_userSettings["isVerified"];
            }
            set
            {
                _userSettings["isVerified"] = value;
            }
        }
        #endregion
    }
}
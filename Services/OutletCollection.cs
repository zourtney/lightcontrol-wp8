﻿using Newtonsoft.Json;
using Services.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Services
{
    public class OutletCollection : BaseCollection<Outlet>
    {
        public OutletCollection(string baseUrl)
            : base(baseUrl)
        { }

        #region Properties
        public override string Url
        {
            get { return "outlets/?t=" + DateTime.Now.Ticks; }
        }
        #endregion
    }
}

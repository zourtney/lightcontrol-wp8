﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class RequestState
    {
        public RequestState(WebRequest request)
        {
            this.Request = request;
        }

        public WebRequest Request { get; protected set; }
    }
}

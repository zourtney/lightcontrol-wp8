﻿using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services.Base
{
    public abstract class BaseCollection<T> : Dictionary<string, T>, IFetchable, ISavable
        where T : BaseModel
    {
        protected HttpClient _client;

        public BaseCollection(string baseUrl)
        {
            _client = new HttpClient();
            BaseUrl = baseUrl;
        }

        public virtual void Populate(string responseContent)
        {
            var deserializedList = JsonConvert.DeserializeObject<List<T>>(responseContent);
            
            this.Clear();
            foreach (var obj in deserializedList)
            {
                obj.BaseUrl = BaseUrl;
                this.Add(obj.ID, obj);
            }
        }

        public virtual async Task<bool> Fetch()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, BaseUrl + "/" + Url);
            HttpResponseMessage response = await _client.SendAsync(request);
            string responseContent = await response.Content.ReadAsStringAsync();
            Populate(responseContent);
            return true;
        }

        public virtual async Task<bool> Fetch(int numRetries)
        {
            var numTries = 0;

            while (numTries < numRetries)
            {
                try
                {
                    await Fetch();
                    return true;
                }
                catch
                {
                    ++numTries;
                }
            }

            return false;
        }

        public virtual async Task<bool> Save()
        {
            string json = JsonConvert.SerializeObject(this.Values);
            HttpContent content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            HttpResponseMessage response = await _client.PutAsync(BaseUrl + "/" + Url, content);
            string responseContent = await response.Content.ReadAsStringAsync();
            Populate(responseContent);
            return true;
        }

        #region Properties
        public string BaseUrl { get; set; }
        public abstract string Url { get; }
        #endregion
    }
}

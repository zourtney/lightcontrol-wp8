﻿using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services.Base
{
    public abstract class BaseModel : /*IFetchable,*/ ISavable
    {
        protected HttpClient _client;

        public BaseModel(string baseUrl)
        {
            _client = new HttpClient();
            BaseUrl = baseUrl;
        }

        public virtual void Populate(string content)
        {
            JsonConvert.PopulateObject(content, this);
        }

        public async Task<bool> Save()
        {
            string json = JsonConvert.SerializeObject(this);
            HttpContent content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            HttpResponseMessage response;
            if (IsNew)
                response = await _client.PostAsync(BaseUrl + "/" + Url, content);
            else
                response = await _client.PutAsync(BaseUrl + "/" + Url, content);

            string responseContent = await response.Content.ReadAsStringAsync();
            Populate(responseContent);
            return true;
        }

        public async Task<bool> Delete()
        {
            HttpResponseMessage response = await _client.DeleteAsync(BaseUrl + "/" + Url);
            return true;
        }

        #region Properties
        public virtual string BaseUrl { get; set; }
        public virtual bool IsNew { get { return false; } }
        public abstract string Url { get; }

        [JsonProperty("id")]
        public virtual string ID { get; set; }
        #endregion
    }
}

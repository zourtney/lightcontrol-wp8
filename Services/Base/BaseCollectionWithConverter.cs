﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Base
{
    public abstract class BaseCollection<T, C> : BaseCollection<T>
        where T : BaseModel
        where C : new()  //JsonConverter, new()  <-- doesn't work! Not sure why, but: http://stackoverflow.com/questions/13254221/windows-phone-8-json
    {
        public BaseCollection(string baseUrl)
            : base(baseUrl)
        { }

        public override void Populate(string responseContent)
        {
            var converter = (JsonConverter)Activator.CreateInstance(typeof(C));
            var deserializedList = JsonConvert.DeserializeObject<List<T>>(responseContent, converter);

            this.Clear();
            foreach (var obj in deserializedList)
            {
                obj.BaseUrl = BaseUrl;
                this.Add(obj.ID, obj);
            }
        }
    }
}

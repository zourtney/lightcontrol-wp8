﻿using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services.Base
{
    public abstract class BaseModel<C> : BaseModel
        where C : IPopulateObjectJsonConverter, new()
    {
        public BaseModel(string baseUrl)
            : base(baseUrl)
        { }

        public override void Populate(string content)
        {
            var converter = (IPopulateObjectJsonConverter)Activator.CreateInstance<C>();
            converter.PopulateObject(content, this);
        }
    }
}

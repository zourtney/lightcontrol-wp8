LightControl WP8
================

A simple Windows Phone 8 app to control the switch states in the Raspberry Pi [LightControl project](https://zourtney@bitbucket.org/zourtney/lightcontrol.git).
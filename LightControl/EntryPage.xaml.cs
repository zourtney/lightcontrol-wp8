﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace LightControl
{
    // Empty entry page. Navigation logic lives in Helpers/FirstPageUriMapper.
    // See http://stackoverflow.com/a/19229374/311207
    public partial class EntryPage : PhoneApplicationPage
    {
        public EntryPage()
        {
            InitializeComponent();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ViewModels;
using ViewModels.Structure;
using ViewModels.Schedules;

namespace LightControl.Controls
{
    public partial class ScheduleListItem : UserControl
    {
        public ScheduleListItem()
        {
            InitializeComponent();
            Loaded += ScheduleListItem_Loaded;
            Unloaded += ScheduleListItem_Unloaded;
        }

        ~ScheduleListItem()
        {
            try
            {
                Loaded -= ScheduleListItem_Loaded;
                Unloaded -= ScheduleListItem_Unloaded;
            }
            catch
            {
                // Wasn't very exceptional. Usually, this destructor won't get
                // called until the application has exited and the UI thread
                // is destroyed (or something). Whatever the case, it throws an
                // InvalidAccessException. Meh.
            }
        }

        void ScheduleListItem_Unloaded(object sender, RoutedEventArgs e)
        {
            Mediator.Unregister("ScheduleDestroyRequested", OnDestroyRequested);
        }

        void ScheduleListItem_Loaded(object sender, RoutedEventArgs e)
        {
            Mediator.Register("ScheduleDestroyRequested", OnDestroyRequested);
        }

        #region Mediator Callbacks
        private void OnDestroyRequested(object sender)
        {
            var schedule = DataContext as ScheduleViewModel;
            if (schedule == sender)
            {
                var result = MessageBox.Show(schedule.RequestDeleteContextText, schedule.RequestDeleteTitleText, MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                    schedule.Delete.Execute(this);
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using LightControl.Resources;
using ViewModels.Structure;
using ViewModels;

namespace LightControl
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            DataContext = App.ViewModel.Outlets;
        }

        ~MainPage()
        {
            Mediator.Unregister("FetchOutletsFailed", ShowFetchError);  //justincase (should be removed in `OnNavigateFrom`)
        }

        #region Mediator Callbacks
        private void ShowFetchError(object sender)
        {
            MessageBox.Show("Unable to get status from server. Please check your settings.");  //TODO: this --> + App.ViewModel.Settings.ServerAddress);
        }
        #endregion

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Mediator.Register("FetchOutletsFailed", ShowFetchError);
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            Mediator.Unregister("FetchOutletsFailed", ShowFetchError);
            base.OnNavigatedFrom(e);
        }

        #region Appbar Event Handling Schenanigans
        private void AppBarRefresh_Click(object sender, EventArgs e)
        {
            Mediator.NotifyColleagues("FetchOutlets", this);
        }

        private void AppBarSettings_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
        }

        private void AppBarAbout_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.Relative));
        }
        
        private void AppBarCalendar_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Schedules.xaml", UriKind.Relative));
        }
        #endregion

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}
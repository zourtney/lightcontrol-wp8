﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace LightControl.Helpers
{
    public class FirstPageUriMapper : UriMapperBase
    {
        protected IsolatedStorageSettings _settings = IsolatedStorageSettings.ApplicationSettings;
                
        public override Uri MapUri(Uri uri)
        {
            if (uri.OriginalString == "/EntryPage.xaml")
                return App.ViewModel.FirstPage;
            return uri;
        }
    }
}

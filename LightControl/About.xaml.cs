﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace LightControl
{
    public partial class About : PhoneApplicationPage
    {
        public About()
        {
            InitializeComponent();
            DataContext = App.ViewModel;
        }

        private void ServerSoftwareLink_Click(object sender, RoutedEventArgs e)
        {
            var task = new WebBrowserTask();
            task.Uri = new Uri("http://bitbucket.org/zourtney/lightcontrol");
            task.Show();
        }

        private void StartTutorial_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Tutorial.xaml", UriKind.Relative));
        }
    }
}
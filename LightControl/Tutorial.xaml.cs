﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.IO.IsolatedStorage;
using System.Windows.Media.Animation;
using ViewModels.Tutorial;
using ViewModels;

namespace LightControl.Tutorial
{
    public partial class Tutorial : PhoneApplicationPage
    {
        private TutorialViewModel _viewModel;

        public Tutorial()
        {
            _viewModel = new TutorialViewModel();
            DataContext = _viewModel;

            InitializeComponent();
            _viewModel.NumPages = TutorialPages.Children.Count;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            Mediator.Register("MovePreviousTutorialPage", OnMoveBackward);
            Mediator.Register("MoveNextTutorialPage", OnMoveForward);
            Mediator.Register("SkipTutorial", OnSkip);
            Mediator.Register("FinishTutorial", OnFinish);

            // Move from page 0 (blank) to 1 (welcome)
            _viewModel.MoveNext.Execute(null);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            Mediator.Unregister("MovePreviousTutorialPage", OnMoveBackward);
            Mediator.Unregister("MoveNextTutorialPage", OnMoveForward);
            Mediator.Unregister("SkipTutorial", OnSkip);
            Mediator.Unregister("FinishTutorial", OnFinish);
            base.OnNavigatedFrom(e);
        }

        private void OnServerLink_Click(object sender, RoutedEventArgs e)
        {
            var task = new WebBrowserTask();
            task.Uri = new Uri("http://bitbucket.org/zourtney/lightcontrol");
            task.Show();
        }

        #region Mediator Callbacks
        private void OnMoveBackward(object index)
        {
            ShowPreviousPanel((int)index);
        }

        private void OnMoveForward(object index)
        {
            ShowNextPanel((int)index);
        }

        private void OnSkip(object index)
        {
            MessageBox.Show(_viewModel.SkipMessage, _viewModel.SkipMessageTitle, MessageBoxButton.OK);
            OnFinish(index);
        }

        private void OnFinish(object index)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            NavigationService.RemoveBackEntry();
        }
        #endregion

        #region Panel Manipulation
        private void ShowNextPanel(int current)
        {
            TransitionForwardStoryboard.Stop();
            
            Storyboard.SetTargetName(TransitionForwardOut, "Panel" + (current - 1) + "Transform");
            Storyboard.SetTargetName(TransitionForwardIn, "Panel" + current + "Transform");

            TransitionForwardStoryboard.Begin();
        }

        private void ShowPreviousPanel(int index)
        {
            TransitionBackwardStoryboard.Stop();

            Storyboard.SetTargetName(TransitionBackwardOut, "Panel" + (index + 1) + "Transform");
            Storyboard.SetTargetName(TransitionBackwardIn, "Panel" + index + "Transform");

            TransitionBackwardStoryboard.Begin();
        }
        #endregion
    }
}
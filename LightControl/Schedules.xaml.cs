﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ViewModels;
using ViewModels.Structure;
using ViewModels.Schedules;

namespace LightControl
{
    public partial class Schedules : PhoneApplicationPage
    {
        public Schedules()
        {
            InitializeComponent();
            DataContext = App.ViewModel.Schedules;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Mediator.Register("FetchSchedulesFailed", ShowFetchError);
            Mediator.Register("ShowSchedule", ShowEditSchedule);
            
            // Only fetch if we don't have any schedules cached.
            var schedules = DataContext as SchedulesViewModel;
            if (schedules.Count <= 0)
                Mediator.NotifyColleagues("FetchSchedules", this);
            
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            Mediator.Unregister("FetchSchedulesFailed", ShowFetchError); 
            Mediator.Unregister("ShowSchedule", ShowEditSchedule);
            base.OnNavigatedFrom(e);
        }

        #region Mediator Callbacks
        private void ShowFetchError(object sender)
        {
            MessageBox.Show("Unable to get schedules from server. Please check your settings.");
        }

        private void ShowEditSchedule(object sender)
        {
            var schedule = sender as ScheduleViewModel;
            if (schedule != null)
                NavigationService.Navigate(new Uri("/EditSchedule.xaml?id=" + Uri.EscapeUriString(schedule.Name), UriKind.Relative));
        }
        #endregion

        #region Appbar Event Handling Schenanigans
        private void AppBarAdd_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/EditSchedule.xaml", UriKind.Relative));
        }

        private void AppBarRefresh_Click(object sender, EventArgs e)
        {
            Mediator.NotifyColleagues("FetchSchedules", this);
        }
        #endregion
    }
}
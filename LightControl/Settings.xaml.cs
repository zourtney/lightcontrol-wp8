﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using LightControl.Helpers;
using ViewModels.Settings;
using ViewModels;

namespace LightControl
{
    public partial class Settings : PhoneApplicationPage
    {
        private SettingsViewModel _viewModel;
        
        public Settings()
        {
            InitializeComponent();
            _viewModel = new SettingsViewModel();
            DataContext = _viewModel;
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            UIUtils.FocusedTextBoxUpdateSource();
            Mediator.NotifyColleagues("FetchOutlets", this);
            base.OnNavigatingFrom(e);
        }
    }
}
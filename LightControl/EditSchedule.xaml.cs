﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using LightControl.Controls;
using ViewModels.Structure;
using LightControl.Helpers;
using Microsoft.Phone.Tasks;
using ViewModels.Schedules;
using ViewModels;

namespace LightControl
{
    public partial class EditSchedule : PhoneApplicationPage
    {
        ApplicationBarIconButton _saveButton;
        ApplicationBarIconButton _deleteButton;
        bool _isSavedOrDeleted = false;

        public EditSchedule()
        {
            InitializeComponent();
            _saveButton = ApplicationBar.Buttons[0] as ApplicationBarIconButton;
            _deleteButton = ApplicationBar.Buttons[1] as ApplicationBarIconButton;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            ScheduleViewModel schedule = null;
            if (NavigationContext.QueryString.ContainsKey("id"))
            {
                // Load schedule by query string parameter
                var name = NavigationContext.QueryString["id"];
                schedule = App.ViewModel.Schedules.FirstOrDefault(vm => vm.Name == name);
            }
            else
            {
                // Create a blank one (new schedule)
                schedule = App.ViewModel.Schedules.CreateNew();
            }

            DataContext = schedule;
            schedule.PropertyChanged += schedule_PropertyChanged;
            _saveButton.IsEnabled = schedule.Name.Length > 0;
            _deleteButton.IsEnabled = !schedule.IsNew;

            Mediator.Register("ScheduleSaved", OnServerOperationSucceeded);
            Mediator.Register("ScheduleSaveFailed", OnSaveFailed);
            Mediator.Register("ScheduleDestroyed", OnServerOperationSucceeded);
            Mediator.Register("ScheduleDestroyFailed", OnDestroyFailed);
            Mediator.Register("LongOperationStarted", OnLongOperationStarted);
            Mediator.Register("LongOperationFinished", OnLongOperationFinished);
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            var schedule = DataContext as ScheduleViewModel;
            schedule.PropertyChanged -= schedule_PropertyChanged;

            if (!_isSavedOrDeleted)
                schedule.Revert.Execute(this);

            Mediator.Unregister("ScheduleSaved", OnServerOperationSucceeded);
            Mediator.Unregister("ScheduleSaveFailed", OnSaveFailed);
            Mediator.Unregister("ScheduleDestroyed", OnServerOperationSucceeded);
            Mediator.Unregister("ScheduleDestroyFailed", OnDestroyFailed);
            Mediator.Unregister("LongOperationStarted", OnLongOperationStarted);
            Mediator.Unregister("LongOperationFinished", OnLongOperationFinished);

            base.OnNavigatingFrom(e);
        }

        #region Event Callbacks
        void schedule_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var schedule = sender as ScheduleViewModel;

            if (e.PropertyName == "Name")
                _saveButton.IsEnabled = schedule.Name.Length > 0;
            else if (e.PropertyName == "IsNew")
                _deleteButton.IsEnabled = !schedule.IsNew;
        }
        #endregion

        #region Mediator Callbacks
        private void OnServerOperationSucceeded(object sender)
        {
            _isSavedOrDeleted = true;
            NavigationService.GoBack();
        }
        
        private void OnSaveFailed(object sender)
        {
            MessageBox.Show("Unexpected error occurred while saving. Please try again.");
        }

        private void OnDestroyFailed(object sender)
        {
            MessageBox.Show("Unexpected error occured while deleting. Please try again.");
        }

        private void OnLongOperationStarted(object obj)
        {
            if (_saveButton != null)
                _saveButton.IsEnabled = false;
            if (_deleteButton != null)
                _deleteButton.IsEnabled = false;
        }

        private void OnLongOperationFinished(object obj)
        {
            if (_saveButton != null)
                _saveButton.IsEnabled = true;
            if (_deleteButton != null)
                _deleteButton.IsEnabled = true;
        }
        #endregion

        //
        //TODO: 'save' button implies that these changes are not saved -- but they are.
        //      They're saved to the view model. Figure out a good way to revert.
        //
        //      Also, the 'alarms' app disabled the save button when nothing has changed.
        //      So obviously they're keeping the original view model and a copy of it,
        //      and setting a flag based on whether or not any field differs.

        #region Appbar Event Handling Schenanigans
        private void AppBarSave_Click(object sender, EventArgs e)
        {
            UIUtils.FocusedTextBoxUpdateSource();
            var schedule = DataContext as ScheduleViewModel;
            schedule.Save.Execute(App.ViewModel.Schedules);
        }

        private void AppBarDelete_Click(object sender, EventArgs e)
        {
            var schedule = DataContext as ScheduleViewModel;
            var result = MessageBox.Show(schedule.RequestDeleteContextText, schedule.RequestDeleteTitleText, MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
                schedule.Delete.Execute(App.ViewModel.Schedules);
        }

        private void CronHelpButton_Click(object sender, RoutedEventArgs e)
        {
            var task = new WebBrowserTask();
            task.Uri = new Uri("http://en.wikipedia.org/wiki/Cron#Examples");
            task.Show();
        }
        #endregion
    }
}